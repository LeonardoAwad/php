<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$clima = 'gelado';

if($clima == 'gelado'){
    
    echo "<br><br> Meu pé congela!<br><br>";

}

$bool = '1';

$var = $bool == 1 ? '$bool é igual a 1' : '$bool é diferente de true';

echo $var . '<br>';

$var = $bool === 1 ? '$bool é igual a 1' : '$bool é diferente de true';

echo $var . '<br>';

$var = $bool != 1 ? '$bool é diferente de 1' : '$bool é diferente de true';

echo $var . '<br>';

$frase = 'macaco careca';

if ( strpos( $frase, 'macaco') !== false){
    echo "encontrei o macaco";
}
else{
    echo "não encontrei o macaco";
}

echo '<br>';
// switch
switch ($clima) {
    case 'quente':
    case 'tropical':
        echo 'adoro clima quente';
        if(true){
            echo '<br> -- if dentro do switch';
            if(true){
                echo '<br> --- if aninhado dentro';
            }
        }
    break;
    
    case 'morno':
        echo 'morno é melhor que frio';
    break;
    
        case 'gelado':
        echo 'gelado é ruim';
    break;
    
    default:
        echo 'so sei que nada sei sobre o tempo';
    break;
}
$isso = 'mano';
//condicional
$isso = $isso ?? 'anão';

echo "<br><br>olha olha $isso";

