<?php

function operacao(float $num1, float $num2, string $operacao = '+'): float
{
    switch ( $operacao) {
        case '+':
            return $num1 + $num2;
        case '-':
            return $num1 - $num2;
        case '*':
            return $num1 * $num2;
        case '/':
            return $num1 / $num2;
        default:
        return 0.0;
    }
}
echo "o resultado é: " . operacao(2.5,2.5, '*'). "<br>";

function dia_da_semana(int $dia): string
{
    $semana= ['Domingo', 'Segunda','Terca','Quarta','Quinta','Sexta','Sabado'];
    return $semana[$dia] ?? 'use de 0 a 6';
}
echo "o dia é: ". dia_da_semana(1);

$nome = 'zezinho';
//passagem de parametro por referencia, o &;-) ( muda o valor da variavel fora do escopo)
function muda_nome(string &$var): string{
    $var = 'outro nome';
    return $var;
}
echo "a função retornara: ".muda_nome($nome). "<br>";

echo"A variavel \$nome ainda vele: \"$nome\" <br>" ;




?>