<?php 

date_default_timezone_set('America/Sao_Paulo');

for ($i = 0, $j = 4; $i < 5 ; $i++ , $j--){
    echo "Linha: $i $j<br>";
}
echo '<br> <br>';

// while

$i = 0;
while ($i < 5){
    echo "linha: $i <br>";
    $i++;
}

// do while
$i = 0;
do{
    echo "linha: $i <br>";
    $i++;
} while ( $i < 5);

// sem vetor
$domingo = 0;
$segunda = 1;
$terca = 2;
$quarta = 3;
$quinta = 4;
$sexta = 5;
$sabado = 6;

var_dump(date("d/m/Y"));

switch (date('w')) {
    case $domingo:
        echo 'domindo';
        break;
    
    case $segunda:
        echo 'segunda';
        break;
}

/*if (date("w")== 4){
    echo "Quinta";
}*/

// com vetor
$semana[0] = 'Domingo';
$semana[1] = 'Segunda';
$semana[2] = 'Terca';
$semana[3] = 'Quarta';
$semana[4] = 'Quinta';
$semana[5] = 'Sexta';
$semana[6] = 'Sabado';

$hoje = date('w');
echo " <br>hoje é " . $semana[$hoje];
